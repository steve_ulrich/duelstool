﻿using UnityEngine;
using System.Collections;
using GameSparks.Api.Requests;
using GameSparks.Api.Messages;

public class NetworkCommunicationManager : Singleton<NetworkCommunicationManager>
{
    private string userID = string.Empty;
    //private string lastUploadId;

    private void Start()
    {
        UploadCompleteMessage.Listener += GetUploadMessage;
    }

    public void AuthenticateUser(System.Action<bool> onComplete)
    {
        new GameSparks.Api.Requests.DeviceAuthenticationRequest().SetDisplayName("Anon").Send((response) => {

            userID = response.UserId;

            bool wasSuccessful = false;
            if (!response.HasErrors)
            {
                Debug.Log("Device Authenticated...");
                wasSuccessful = true;
            }
            else
            {
                Debug.Log("Error Authenticating Device...");
                wasSuccessful = false;

            }

            if (onComplete != null)
            {
                onComplete.Invoke(wasSuccessful);
            }
        });
    }

    #region Upload File

    public void UploadFile(byte[] file)
    {
        new GetUploadUrlRequest().Send((response) =>
        {
            //Start coroutine and pass in the upload url
            StartCoroutine(UploadAFile(response.Url, file));
        });
    }

    public IEnumerator UploadAFile(string uploadUrl, byte[] file)
    {
        //yield return new WaitForEndOfFrame();
        //GameSparks.Api.GSHelper.GetUploadUrlRequestAndUploadFile(file, userID + "_" + Backend.Utility.Now() + ".png");
        yield return new WaitForEndOfFrame();

        // Create a Web Form, this will be our POST method's data
        var form = new WWWForm();

        // When we upload a poop, set some piece of data on it that says it hasn't been judged
        form.AddField("judgement", 0);
        form.AddBinaryData("file", file, userID + "_" + Backend.Utility.Now().ToShortDateString() + ".png", "image /png");

        //POST the screenshot to GameSparks
        UnityEngine.Networking.UnityWebRequest w = UnityEngine.Networking.UnityWebRequest.Post(uploadUrl, form);

        yield return w;

        if (w.error != null)
        {
            Debug.Log(w.error);
        }
        else
        {
            Debug.Log(w.ToString());
        }
    }

    public void GetUploadMessage(GSMessage message)
    {
        //Every time we get a message
        string uploadId = (message.BaseData.GetString("uploadId"));

        new LogEventRequest().SetEventKey("ADD_POOP").SetEventAttribute("uploadId", uploadId).SetEventAttribute("date", Backend.Utility.Now().ToShortDateString()).Send((response) => 
        {
            if(response.HasErrors)
            {
                Debug.Log(response.Errors.ToString());
            }
            else
            {
                Debug.Log("ADD_POOP finished with no errors");
            }
        });
    }

    #endregion

    #region Download File

    public void GetDownloadURL()
    {
        System.Collections.Generic.List<string> uploadIds = new System.Collections.Generic.List<string>();

        new LogEventRequest().SetEventKey("REQUEST_POOPS").SetEventAttribute("date", Backend.Utility.Now().ToShortDateString()).Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log(response.Errors.ToString());
            }
            else
            {
                Debug.Log(response.JSONString);

                Debug.Log("REQUEST_POOP finished with no errors");
            }
        });
    }

    #endregion

}
