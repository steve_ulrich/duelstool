﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return Backend.Utility.WaitForFrame;

        Debug.Log(Backend.Utility.Now().ToShortDateString());

        bool successfulAuthentication = false;
        bool userInitComplete = false;
        System.Action<bool> onUserInitialized = delegate (bool success)
        {
            successfulAuthentication = success;
            userInitComplete = true;
        };
        // Authenticate Device
        Debug.Log("Bootstrap| Authentication Test");
        NetworkCommunicationManager.Instance.AuthenticateUser(onUserInitialized);
        while (userInitComplete == false)
        {
            yield return Backend.Utility.WaitForFrame;
        }

        if (successfulAuthentication)
        {
            bool dpDownloaded = false;
            bool dpGrabbed = false;
            // Setup a callback that will handle a true or false result from our internet connection test
            System.Action<bool> cbOnDynamicPropertiesInit = delegate (bool connectionStatus)
            {
                if (connectionStatus)
                {
                    Debug.Log("Connection Test Successful!");
                    dpGrabbed = true;
                }
                else
                {
                    Debug.Log("***Connection Test NOT Successful!");
                    dpGrabbed = false;
                }

                dpDownloaded = true;
            };

            Debug.Log("Bootstrap| Dynamic Property");
            // Pull dynamic properties
            yield return Backend.DynamicProgramProperties.Initialize(cbOnDynamicPropertiesInit);

            while (dpDownloaded == false)
            {
                yield return Backend.Utility.WaitForFrame;
            }

            // If we have internet, download a set of dynamic program properties from the cloud
            if (dpGrabbed)
            {
                Debug.Log("Bootstrap| Connection Test Successful");
                // Enable SR Debugger on development builds ALWAYS
                if (Debug.isDebugBuild)
                {
                    SRDebug.Init();
                }
                else // Otherwise check our DP to see if we should enable it for release builds
                {
                    bool shouldInitSRDebugger = false;
                    Backend.DynamicProgramProperties.TryGetValue<bool>("DebuggerEnabled", out shouldInitSRDebugger);

                    if (shouldInitSRDebugger)
                    {
                        SRDebug.Init();
                    }
                }

                // Grab yesterday's winner
                Debug.Log("Bootstrap| Authentication Test Successful");


                // Go to main menu
                SceneManager.Instance.LoadScene("Main");
            }
            // If we don't have internet, notify the user that this app requires internet etc etc.
            else
            {
                Backend.Utility.MakeNewGenericPopup("ERROR", "Unable to grab properties. Please check your connection and restart the app", false);
                Debug.Log("Error, no connection");
                yield break;
            }
        }
        else
        {
            Backend.Utility.MakeNewGenericPopup("ERROR", "Unable to authenticate your device. Please restart the app!", false);
            Debug.Log("Error, no connection");
            yield break;
        }
        
        
    }
}
