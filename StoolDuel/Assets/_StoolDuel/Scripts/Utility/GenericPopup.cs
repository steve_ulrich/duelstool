﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GenericPopup : MonoBehaviour
{
    public Animator Anim;

    public TMPro.TextMeshProUGUI TitleText;
    public TMPro.TextMeshProUGUI MessageText;

    public Button CancelButton;
    public TMPro.TextMeshProUGUI CancelButtonText;
    public Button OkButton;
    public TMPro.TextMeshProUGUI OkButtonText;

    private int closePopupAnimHash;

    private void OnEnable()
    {
        closePopupAnimHash = Animator.StringToHash("ClosePopup");
    }

    public void ClosePopup()
    {
        Anim.Play(closePopupAnimHash);
    }

    public void DestroyPopup()
    {
        Destroy(this.gameObject);
    }

}
