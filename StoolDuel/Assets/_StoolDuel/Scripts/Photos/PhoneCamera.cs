﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneCamera : MonoBehaviour
{
    public PhotoUIManager photoUIManager;

    private bool camAvailable;
    private WebCamTexture backCam;
    private WebCamTexture frontCam;
    private WebCamTexture currentCam;
    private Texture defaultBackground;

    public UnityEngine.UI.RawImage background;
    public UnityEngine.UI.AspectRatioFitter fit;
    public GameObject swapButton;
    public UnityEngine.UI.Image flashPanel;

    public float flashTime = 0.25f;

    private void Awake()
    {
        if(photoUIManager == null)
        {
            photoUIManager = GetComponent<PhotoUIManager>();
        }

        flashPanel.enabled = false;

        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("Camera NOT detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
            else
            {
                frontCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (backCam == null)
        {
            currentCam = frontCam;
            swapButton.SetActive(false);
        }

        if (frontCam == null)
        {
            currentCam = backCam;
            swapButton.SetActive(false);

        }

        if (currentCam == null)
        {
            camAvailable = false;
            return;
        }

        currentCam.Play();
        background.texture = currentCam;

        camAvailable = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(!camAvailable)
        {
            return;
        }

        float ratio = (float)currentCam.width / (float)currentCam.height;
        fit.aspectRatio = ratio;

        float scaleY = currentCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -currentCam.videoRotationAngle;

        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
	}

    /// <summary>
    /// Tied to a button on our camera UI
    /// </summary>
    public void OnTakePhotoPressed()
    {
        //Photo taking logic
        StartCoroutine(TakePhoto());
    }

    IEnumerator TakePhoto()
    {
        yield return null;
        Texture2D photo = new Texture2D(currentCam.width, currentCam.height);
        photo.SetPixels(currentCam.GetPixels());
        photo.Apply();

        currentCam.Stop();

        photoUIManager.SetTakenPhoto(photo);

        yield return StartCoroutine(PhotoFlash());

        photoUIManager.ActivatePhotoConfirmationUI();
    }

    IEnumerator PhotoFlash()
    {
        flashPanel.enabled = true;
        flashPanel.color = Color.white;

        flashPanel.CrossFadeColor(Color.clear, flashTime, false, true);

        float currentTimer = 0f;
        while(currentTimer < flashTime)
        {
            currentTimer += Time.deltaTime;
            yield return null;
        }

        flashPanel.enabled = false;
    }

    /// <summary>
    /// Tied to a button on our camera UI
    /// </summary>
    public void SwapCamera()
    {
        if(currentCam == backCam && frontCam != null)
        {
            currentCam = frontCam;
            currentCam.Play();
        }
        else if(currentCam == frontCam && backCam != null)
        {
            currentCam = backCam;
            currentCam.Play();
        }
    }
}
