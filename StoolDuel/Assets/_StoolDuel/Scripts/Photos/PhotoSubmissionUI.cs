﻿using UnityEngine;
using System.Collections;

public class PhotoSubmissionUI : MonoBehaviour
{
    public GameObject[] progressDots;

    public GameObject submissionJudgingHolder;
    public GameObject submissionThanksHolder;

    public UnityEngine.UI.RawImage topImage;
    public UnityEngine.UI.RawImage bottomImage;
    public UnityEngine.UI.RawImage submissionImage;

    public PhotoUIManager photoUIManager;

    public int currentProgress = 0;

    private void OnEnable()
    {
        currentProgress = 0;

        submissionJudgingHolder.SetActive(true);
        submissionThanksHolder.SetActive(false);

        progressDots[currentProgress].SetActive(true);
        // increment the progress, activate a new dot.
        currentProgress++;

        // Grab 2 poop pics from the cloud and populate our top/bottom images
        GetNewPoops();
    }

    public void OnPressImage(int imageIndex)
    {
        progressDots[currentProgress].SetActive(true);
        // increment the progress, activate a new dot.
        currentProgress++;

        if(currentProgress >= progressDots.Length)
        {
            currentProgress = 0;
            submissionJudgingHolder.SetActive(false);
            submissionThanksHolder.SetActive(true);

            // Submit our image to the cloud for consideration!
            //Encode to a PNG
            byte[] bytes = photoUIManager.takenPhoto.EncodeToPNG();

            //Write out the PNG. Of course you have to substitute your_path for something sensible
            System.IO.File.WriteAllBytes(Application.dataPath + "/_StoolDuel/SavedScreen.png", bytes);

            NetworkCommunicationManager.Instance.UploadFile(bytes);

        }
        else
        {
            // Get a new photo!
            GetNewPoops();
        }

    }

    private void GetNewPoops()
    {
        // Grab a poop for the top image
        NetworkCommunicationManager.Instance.GetDownloadURL();
        // Grab a poop for the bottom image
    }

    public void ReturnToMenu()
    {
        SceneManager.Instance.LoadScene("Main");
    }
}
