﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoUIManager : MonoBehaviour
{
    public GameObject takePhotoUI;
    public PhotoConfirmationUI photoConfirmationUI;
    public PhotoSubmissionUI submissionUI;

    [System.NonSerialized]
    public GameObject currentUI;

    [System.NonSerialized]
    public Texture2D takenPhoto;

    private void Awake()
    {
        currentUI = takePhotoUI;

        TurnOffAllUI();

        ActivateTakePhotoUI();
    }

    public void SetTakenPhoto(Texture2D photo)
    {
        takenPhoto = photo;

        photoConfirmationUI.takenImage.texture = photo;
        submissionUI.submissionImage.texture = photo;
    }

    public void ActivateTakePhotoUI()
    {
        ActivateNewUI(takePhotoUI);
    }

    public void ActivatePhotoConfirmationUI()
    {
        ActivateNewUI(photoConfirmationUI.gameObject);
    }

    public void ActivateSubmissionUI()
    {
        ActivateNewUI(submissionUI.gameObject);
    }

    void ActivateNewUI(GameObject targetUI)
    {
        currentUI.SetActive(false);

        targetUI.SetActive(true);

        currentUI = targetUI;
    }

    void TurnOffAllUI()
    {
        takePhotoUI.SetActive(false);
        photoConfirmationUI.gameObject.SetActive(false);
        submissionUI.gameObject.SetActive(false);
    }

}
