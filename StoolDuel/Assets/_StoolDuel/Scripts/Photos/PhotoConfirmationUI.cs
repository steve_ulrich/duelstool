﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoConfirmationUI : MonoBehaviour
{
    public UnityEngine.UI.RawImage takenImage;

    public PhotoUIManager photoUIManager;

    
    public void RetakePhoto()
    {
        photoUIManager.ActivateTakePhotoUI();
    }

    public void SubmitPhoto()
    {
        photoUIManager.ActivateSubmissionUI();
    }
}
